﻿using System;
using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;

namespace BenchmarkTest
{
    public class CollectionAnalyzer
    {
        private const int N = 1000;

        private Dictionary<SimpleModel, decimal> _dict;

        private List<NotSoSimpleModel> _list;

        public CollectionAnalyzer()
        {
            var randomiser = new Random();
            _dict = new Dictionary<SimpleModel, decimal>();
            _list = new List<NotSoSimpleModel>();
            for (var i = 0; i < N; i++)
            {
                var randomInt = randomiser.Next();
                var randomInt2 = randomiser.Next();
                var randomDecimal = (decimal)new Random(N).NextDouble();
                _dict.Add(new SimpleModel(randomInt, randomInt2), randomDecimal);
                _list.Add(new NotSoSimpleModel(randomInt, randomInt2, randomDecimal));
            }
        }

        public IEnumerable<SimpleModel> Keys()
        {
            var randomiser = new Random();
            yield return _dict.Keys.ToList()[randomiser.Next(N)];
            yield return _dict.Keys.ToList()[randomiser.Next(N)];
            yield return _dict.Keys.ToList()[randomiser.Next(N)];
            yield return _dict.Keys.ToList()[randomiser.Next(N)];
        }

        [Benchmark]
        [ArgumentsSource(nameof(Keys))]
        public decimal FindDecimalInTheDictionary(SimpleModel key)
        {
            return _dict.TryGetValue(key, out var value) ? value : 0;
        }

        [Benchmark]
        [ArgumentsSource(nameof(Keys))]
        public decimal FindDecimalInTheList(SimpleModel key)
        {
            return _list.First(value => value.A == key.A && value.B == key.B).C;
        }
    }
}
