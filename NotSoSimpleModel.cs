﻿namespace BenchmarkTest
{
    public class NotSoSimpleModel
    {
        public NotSoSimpleModel(int a, int b, decimal c)
        {
            A = a;
            B = b;
            C = c;
        }
        public int A { get; set; }
        public int B { get; set; }
        public decimal C { get; set; }
    }
}
