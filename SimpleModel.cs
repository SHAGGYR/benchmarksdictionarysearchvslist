﻿namespace BenchmarkTest
{
    public class SimpleModel
    {
        public SimpleModel(int a, int b)
        {
            A = a;
            B = b;
        }
        public int A { get; set; }
        public int B { get; set; }
    }
}
